import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Grades {
    private int id;
    private float value;

    public Grades() {
    }

    public Grades(int id, float value) {
        this.id = id;
        this.value = value;
    }

    //Getters and setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }
}
