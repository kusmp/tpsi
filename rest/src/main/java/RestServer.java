import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RestServer {

    public static void main(String[] args){
        List<Student> students = new ArrayList<Student>();
        Date date = new Date();
        List<Grades> grades = new ArrayList<>();

        SimpleDateFormat dataFormatter = new SimpleDateFormat("yyyy-mm-dd");
        students.add(new Student("Jan", "Kowalski", 122, date, grades));
        students.add(new Student("Jan", "Nowak", 125, date, grades));
        students.add(new Student("Marian", "Kwiatkowski", 124, date, grades));

    }

}
