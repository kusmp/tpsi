import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class ServerProxy {
    public static void main(String[] args) throws Exception {
        int port = 8001;
        HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
        server.createContext("/", new RootHandler());
        System.out.println("Starting server on port: " + port);
        server.start();
    }
    static class RootHandler implements HttpHandler {
        public void handle(HttpExchange exchange) throws IOException {
            HttpURLConnection conn = (HttpURLConnection)exchange.getRequestURI().toURL().openConnection();
            try{
             conn = setupConn(exchange);
            readBodyBytes(conn, exchange);
            conn = transferData(exchange, conn);
            }
            catch(Exception e){
                e.printStackTrace();
            }
            finally {
                conn.disconnect();
            }
        }
        /* https://stackoverflow.com/questions/2163644/in-java-how-can-i-convert-an-inputstream-into-a-byte-array-byte */
        public static byte[] readFully(InputStream input) throws IOException
        {
            byte[] buffer = new byte[8192];
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            while ((bytesRead = input.read(buffer)) != -1)
            {
                output.write(buffer, 0, bytesRead);
            }
            return output.toByteArray();
        }

        public HttpURLConnection setupConn(HttpExchange exchange) throws IOException{
            URL url = exchange.getRequestURI().toURL();
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setInstanceFollowRedirects(false);
            String requestMethod = exchange.getRequestMethod();
            conn.setRequestMethod(requestMethod);
            conn.setRequestProperty("Via", "localhost");
            Map<String, List<String>> reqHead = exchange.getRequestHeaders();
            for(Map.Entry<String, List<String>> en : reqHead.entrySet()){
                if (en!= null && en.getKey() != null){
                    for(String value : en.getValue()){
                        conn.setRequestProperty(en.getKey(), value);
                    }
                }
            }
            return conn;
        }

        public void readBodyBytes(HttpURLConnection conn, HttpExchange exchange) throws IOException{
            byte[] reqBytes = null;
            InputStream bodyIs = exchange.getRequestBody();
            byte[] requestBodyBytes = readFully(bodyIs);
            boolean status = exchange.getRequestMethod().equals("POST");
            if(status){
                conn.setDoOutput(true);
                OutputStream os = conn.getOutputStream();
                os.write(requestBodyBytes);
                os.close();
            }
        }

        public HttpURLConnection transferData(HttpExchange exchange, HttpURLConnection conn) throws IOException{
            byte[] response = null;
            InputStream serverResponse;
            if(conn.getResponseCode() >= 400){
                serverResponse = conn.getErrorStream();
            }
            else{
                serverResponse = conn.getInputStream();
            }

            if(serverResponse.available()>0){
                response = readFully(serverResponse);
            }
            Map<String, List<String>> responseHeaders  = conn.getHeaderFields();
            for (Map.Entry<String,List<String>> en: responseHeaders.entrySet()){
                if (en!= null && en.getKey() != null && !en.getKey().toLowerCase().equals("Transfer-Encoding")){
                    for (String value : en.getValue()){
                        exchange.getResponseHeaders().add(en.getKey(), value);
                    }
                }
            }
            exchange.getResponseHeaders().set("Via", "localhost");
            exchange.sendResponseHeaders(conn.getResponseCode(), response.length);
            OutputStream os = exchange.getResponseBody();
            os.write(response);
            os.close();
            return conn;
        }
    }

}
