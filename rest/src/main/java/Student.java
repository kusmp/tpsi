import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@XmlRootElement
public class Student {
    private String name;
    private String surname;
    private int index;
    private Date birthDate;
    private List<Grades> grades;

    public Student() {
    }

    public Student(String name, String surname, int index, Date birthDate, List<Grades> grades) {
        this.name = name;
        this.surname = surname;
        this.index = index;
        this.birthDate = birthDate;
        this.grades = grades;
    }

    //Getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public List<Grades> getGrades() {
        return grades;
    }

    public void setGrades(List<Grades> grades) {
        this.grades = grades;
    }
    //

}
